initial_node = 1;
final_node =5;

open_list = [ struct('id', initial_node, 'parent', 0) ]
closed_list = [];

A = [0 2 0 0 0 1 0 0 0;
      2 0 3 5 0 0 0 0 0;
      0 3 0 1 0 0 0 0 0;
      0 5 1 0 3 0 0 0 0;
      0 0 0 3 0 0 0 0 4;
      1 0 0 0 0 0 2 6 0;
      0 0 0 0 0 2 0 0 1;
      0 0 0 0 0 6 0 0 2;
      0 0 0 0 4 0 1 2 0
      ];
 
 while ~isempty(open_list)
 
 cur_node = open_list(1);
 open_list(1) = [];
 closed_list = [closed_list cur_node];
 
 if cur_node.id == final_node
     disp('I got Final node!')
    break
 end
 
 node_ids = find(A([cur_node.id], :) > 0);
    
 if ~isempty(closed_list)
    node_ids = setdiff(node_ids, [closed_list.id], 'stable');
 end
 
 node_parents = repmat(cur_node.id, size(node_ids));
 hyp_nodes = struct('id', num2cell(node_ids), 'parent', num2cell(node_parents));

 open_list = [ hyp_nodes open_list];
 
 end
 
 cur_id = final_node;
 route = [];
 while cur_id > 0
    route = [cur_id route];
    array_index = find([closed_list.id] == cur_id);
    cur_id = closed_list(array_index(1)).parent;
 end
 route  